///	@file rtKernelBench.c 
///	@brief Kernel module that uses the isolator to benchmark
///        the real time performance of a system
///
///
/// @detail This module with isolate a core through the use of the
///         rts-cpu-isolator, and deploy a real time(rt) loop (real_time_loop 
///         in this file) onto that isolated core. The rt loop will then
///         do work and track how long that work takes across many iterations.
///         The recorded latancies are added to a histogram and statistics are
///         collected for the run. When the module is removed those statistics
///         are printed to dmesg along with a pass/fail result.
///
///         Key Metrics:
///         Jitter - The difference between long runs and the average run time.
///
///
///         Interpreting Results:
///         If the long loop times are significantly longer than the mean we 
///         interpret those measurements as the work being interrupted. This
///         implies that core isolation is not working as expected on the 
///         tested machine. The default parameters allow the largest max 
///         to be 25% longer than the mean, anything longer is a fail.      
///
///
/// @author Ezekiel Dohmen
///
///
//

#include <linux/version.h>
#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <linux/random.h>
#include <linux/kthread.h>             //kernel threads

#include <asm/uaccess.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>
#include <asm/cacheflush.h>

//From the advligorts-cpu-isolator-dkms package
#include "rts-cpu-isolator.h" 

//Local Includes
#include "mstore.h"
#include "hist.h"

MODULE_DESCRIPTION( "Core isolation jitter benchmark" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );


//
// Start Module Parameters
//

// This is the percent larger the max latency is allowed
// to be before failling the benchmark
//
int MP_BENCHMARK_MAX_TO_AVG_PERCENT = 25;
module_param(MP_BENCHMARK_MAX_TO_AVG_PERCENT, int, S_IRUSR);

// This is the time (in sec) that we wait after the real time loop starts
// until we start recording latencies. 
// Only used when MP_AUTO_DETECT_STABILITY = 0 
int MP_ALLOWED_SETTLING_TIME_S = 5;
module_param(MP_ALLOWED_SETTLING_TIME_S, int, S_IRUSR);


// This is the CPU index we would like to run on. -1 means the
// module will let the isolator module choose
int MP_CPU_ID = -1;
module_param(MP_CPU_ID, int, S_IRUSR);

// If this parameter is set (>0), latencies over this value will be recorded
// and printed at the end of the run. Can show periodic issues
int MP_HIGH_THRESH_NS = 0;
module_param(MP_HIGH_THRESH_NS, int, S_IRUSR);

// When MP_HIGH_THRESH_NS is set high values are stored in an circular buffer
// You can change the below value to control how many values can be stored
int MP_HIGH_THRESH_CBUF_SIZE = 100;
module_param(MP_HIGH_THRESH_CBUF_SIZE, int, S_IRUSR);

// 0 Offset in seconds, 1 offset in us
int MP_HIGH_THRESH_FORMAT = 0;
module_param(MP_HIGH_THRESH_FORMAT, int,  S_IRUSR);

// When enabled (>0) the module will attempt to detect when
// the timing stabilizes, not very useful when real time performance
// is bad. 
int MP_AUTO_DETECT_STABILITY = 0;
module_param(MP_AUTO_DETECT_STABILITY, int, S_IRUSR);

// When the mean is measured over this value, it is often
// a sign that the CPU power settings are configured such that
// when a core is isolated is gets powered down to a lower 
// operating frequency. Thus increasing model's processing time.
int MP_EXPECTED_MEAN_NS = 5000;
module_param(MP_EXPECTED_MEAN_NS, int, S_IRUSR);


int MP_RUN_MATH_BENCHMARK = 0;
module_param(MP_RUN_MATH_BENCHMARK, int, S_IRUSR);

int MP_RUN_PRINTK_THREAD = 0;
module_param(MP_RUN_PRINTK_THREAD, int, S_IRUSR);


int MP_PRINTK_THREAD_SLEEP_MS = 100;
module_param(MP_PRINTK_THREAD_SLEEP_MS, int, S_IRUSR);

// End Module Parameters


//
// Start Module Specific defines and vars
//
#define SYSTEM_NAME_STRING_LOWER "advligo-rt-bench" 
#define DIV_NS_TO_SEC 1000000000ULL
#define DIV_NS_TO_MS  1000000ULL
#define DIV_NS_TO_US  1000ULL

//
// Custom Types
//

typedef struct over_thresh_event_t
{
    uint64_t dur_ns;
    uint64_t since_start_ns;
} over_thresh_event_t; //Used to record over thresh events


//
// Globals
//

int g_assigned_cpu = -1; // Holds the assigned CPU from the CPU isolator module
static atomic_t g_atom_should_exit = ATOMIC_INIT(0); // signalling var for when rt loop should exit
static atomic_t g_atom_has_exited = ATOMIC_INIT(0); // signalling var for when rt loop has exited
mstore_context_t * g_mstore_ctx = NULL;
hist_context_t * g_hist_ctx = NULL;

static struct task_struct *g_print_thread=NULL; //Used to print dmesg while test is running

void real_time_loop( void );

#define DATA_LEN 8192
volatile int g_data[DATA_LEN];
volatile int g_res_data[DATA_LEN];


int thread_function(void *pv)
{
    int i=0;
    while(!kthread_should_stop()) {
        printk("Test is a test message used to see if printing effects the systems real-time performance %d\n", i);
        msleep(MP_PRINTK_THREAD_SLEEP_MS);
        ++i;
    }
    return 0;
}


void wait_for_stability( void )
{
    uint64_t start_time_tsc, func_start_time_tsc, last_jitter_tsc; // Timers
    uint64_t diff_ns = 0, runs=0, i;

    timer_start( &func_start_time_tsc );
    timer_start( &last_jitter_tsc );

    while(atomic_read(&g_atom_should_exit) == 0)
    {

        timer_start ( &start_time_tsc );

        if( runs > 10000  //Wait for at least 10000 runs
            && ( start_time_tsc - last_jitter_tsc )/tsc_khz  > 2000  ) // ms
        {
            common_print("It took %llu ms for stability, %llu runs.\n", ( start_time_tsc - last_jitter_tsc )/tsc_khz, runs ); 
            return;
        }

        for( i = 0; i<DATA_LEN; ++i)
        {
            g_data[i] = i;
        }

        diff_ns = timer_end_ns( &start_time_tsc );
        runs++;
        if(diff_ns > 7000)
        {
            timer_start( &last_jitter_tsc );
        }
    }
    common_print("wait_for_stability() - Error did not find stability, signalled to exit.\n");
}



// MAIN routine: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
int rt_fe_init( void )
{
    int ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    int64_t ranges [] = {3000, 4000, 5000, 6000, 7000, 8000, 10000, 12000, 14000, 16000, 18000, 22000, 25000};


    g_mstore_ctx = initialize_mstore(sizeof(over_thresh_event_t), MP_HIGH_THRESH_CBUF_SIZE);
    g_hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    if(g_mstore_ctx == NULL || g_hist_ctx == NULL)
    {
        common_print("%s : Error: The mstore or hist allocation failed...", SYSTEM_NAME_STRING_LOWER);
        while(atomic_read(&g_atom_should_exit) == 0)
        {
            msleep(1);
        }
        return -1;
    }



    if( MP_RUN_PRINTK_THREAD ) {
        g_print_thread = kthread_run(thread_function, NULL,"Printk Thread");
        if(g_print_thread) {
            pr_info("g_print_thread Created Successfully...\n");
        } else {
            pr_err("Cannot create g_print_thread kthread\n");
            return -1;
        }
    }


    ret = rts_isolator_run( real_time_loop, MP_CPU_ID, &g_assigned_cpu);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        common_print(SYSTEM_NAME_STRING_LOWER " : rts-cpu-isolator : %s\n", error_msg);
        return -1;
    }


    return 0;
}

/// Kernel module cleanup function
void rt_fe_cleanup( void )
{
    int ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    uint64_t stop_sig_time_tsc;

    //Stop Kthread if running
    if( g_print_thread ) {
        kthread_stop(g_print_thread);
        msleep( 250 );
    }

    timer_start( &stop_sig_time_tsc );

    // Stop the code and wait
    atomic_set(&g_atom_should_exit, 1);
    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }

    common_print("It took %lld ms for the RT code to exit.\n",
            timer_end_ns(&stop_sig_time_tsc)/DIV_NS_TO_MS);


    ret = rts_isolator_cleanup( g_assigned_cpu );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        common_print(SYSTEM_NAME_STRING_LOWER " : rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
    }

    hist_free(g_hist_ctx);
    free_mstore(g_mstore_ctx);


}

module_init( rt_fe_init );
module_exit( rt_fe_cleanup );

void mcpy_work( void )
{
    //Do work
    for( int i = 0; i<DATA_LEN; ++i)
    {
        g_data[i] = i;
    } 
}

void multiply_work( void )
{
    for( int i = 0; i<DATA_LEN; ++i)
    {
        g_res_data[i] = g_data[i] * i;
    }
}

void real_time_loop( void )
{


    uint64_t start_time_tsc = 0, end_time_tsc = 0,  func_start_time_tsc = 0;
    int64_t diff_ns = 0;
    uint64_t total_time_ns = 0, max_ns = 0, min_ns = 1e12, over_high_thresh = 0;
    uint64_t max_index = 0;
    uint64_t total_count = 0, rollover_count = 0;
    int i;

    if ( MP_RUN_MATH_BENCHMARK )
    {
        for( int i = 0; i<DATA_LEN; ++i) {
            get_random_bytes((void*)&g_data[i], sizeof g_data[0]);
            g_data[i] %= 32564;
        }
    }

    //Record the rt loop start time, so we will know how long we have run
    timer_start ( &func_start_time_tsc );

    over_thresh_event_t event;


    if ( MP_AUTO_DETECT_STABILITY )
        wait_for_stability();
    MP_AUTO_DETECT_STABILITY = 0;


    while(atomic_read(&g_atom_should_exit) == 0)
    {

        timer_start ( &start_time_tsc );


        if ( MP_RUN_MATH_BENCHMARK )
        {
            multiply_work();
        }
        else
        {
            mcpy_work();
        }

        //If we aren't collecting data yet, continue
        if( (MP_AUTO_DETECT_STABILITY == 0) &&
            (timer_diff_ns(&start_time_tsc, &func_start_time_tsc)/DIV_NS_TO_SEC) < MP_ALLOWED_SETTLING_TIME_S
          )
        {
            // If we don't auto-detect timing stability, skip MP_ALLOWED_SETTLING_TIME_S 
            // before collecting data.
            continue;
        }
        else
        {
            timer_start( &end_time_tsc );
            diff_ns = timer_diff_ns( &end_time_tsc, &start_time_tsc);


            if( diff_ns >= 0)
            {
                if(diff_ns < min_ns)
                {
                    min_ns = diff_ns;
                }
                if(diff_ns > max_ns)
                {
                    max_ns = diff_ns;
                    max_index = total_count;
                }

                if( MP_HIGH_THRESH_NS > 0 &&
                    diff_ns > MP_HIGH_THRESH_NS)
                {
                    ++over_high_thresh;
                    event.dur_ns = diff_ns;
                    event.since_start_ns = timer_diff_ns(&end_time_tsc, &func_start_time_tsc);
                    add_element(g_mstore_ctx, &event);
                }

                //Add the latency to the histogram
                hist_add_element(g_hist_ctx, diff_ns);

                total_time_ns += diff_ns;
                ++total_count;

            }
            else
            {
                ++rollover_count;
            }

        }
    }

    if(total_count == 0) total_count = 1; //Make sure we don't div by 0

    common_print( "Total loops: %lld \n"
            "Avg: %lld ns, min: %lld ns, max: %lld ns, "
            "max_index: %lld, max_index_percent: %lld%%,"
            "\nrollover_count: %lld, Over %d ns: %lld\n",
            total_count,
            (total_time_ns/total_count), min_ns, max_ns, 
            max_index,
            (max_index*100)/total_count,
            rollover_count,
            MP_HIGH_THRESH_NS,
            over_high_thresh);


    common_print("Test Total Time (s) %lld\n", timer_diff_ns(&end_time_tsc, &func_start_time_tsc)/DIV_NS_TO_SEC );

    common_print("Histogram Of All Latencies (ns)\n");
    hist_print_stats(g_hist_ctx);

    //If max is less than avg * ( 1 + (MP_BENCHMARK_MAX_TO_AVG_PERCENT/100))
    if( max_ns < ((total_time_ns/total_count) * (100+MP_BENCHMARK_MAX_TO_AVG_PERCENT))/100 ) // Math reordered to avoid floating point calc
    {
        common_print("BENCHMARK PASS\n");
    }
    else
    {
        common_print("BENCHMARK FAIL\n");
    }

    if( (total_time_ns/total_count) > MP_EXPECTED_MEAN_NS )
    {
        common_print("WARNING: The measured mean %d ns is higher than the expected <%d.\n", (int)(total_time_ns/total_count), MP_EXPECTED_MEAN_NS);
        common_print("This could mean your CPU power setting are not set correctly. You may want to poke around your BIOS settings.\n");
    }

    //Print out cbuf over thesh events, if we recorded any
    over_thresh_event_t * event_ptr;
    for(i = 0; i < get_num_elements(g_mstore_ctx); ++i)
    {
        event_ptr = get_element_ptr(g_mstore_ctx, i);
        common_print("%lld, %llu %s\n", 
                     event_ptr->dur_ns, 
                     (MP_HIGH_THRESH_FORMAT == 0 ) ? event_ptr->since_start_ns / DIV_NS_TO_SEC : event_ptr->since_start_ns / DIV_NS_TO_US , 
                     (MP_HIGH_THRESH_FORMAT == 0 ) ? "s" : "us");
    }



    // System reset command received
    atomic_set(&g_atom_has_exited, 1); 
    return;
}


