#define _GNU_SOURCE

#include <time.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <stdint.h>
#include <signal.h>
#include <sys/mman.h>
#include <unistd.h> //getopt

#include <pthread.h>

#include <sched.h>
//#include <linux/getcpu.h>

#include "hist.h"
#include "mstore.h"

const clockid_t clkType =  CLOCK_MONOTONIC;
//const clockid_t clkType =  CLOCK_BOOTTIME;

const uint64_t THRESHOLD_NS = 5000;

const uint64_t TIME_TO_SKIP_NS = 1e9;

static volatile int keepRunning = 1;
void intHandler(int dummy) {
    keepRunning = 0;
}

static uint64_t getTotal_ns(const struct timespec * ts)
{
    return ts->tv_sec*1.0e9 + ts->tv_nsec;
}

static void do_work(uint64_t num_items)
{
    uint64_t count, sum = 0;
    static volatile uint64_t result = 0;

    for(count = 0; count < num_items; ++count)
    {
        sum += count; 
    }
    result = sum;

}

typedef struct latency_and_time_t
{
    int64_t  latency_ns;
    uint64_t time_ns;
    uint64_t run_num;
} latency_and_time_t;

int main(int argc, char** argv)
{
    static struct timespec last_time, now_time, start_rec_time;
    uint64_t total_time_ns = 0, max_ns = 0, min_ns = 1e12, max_index=0;
    uint64_t over_thresh_count = 0;
    uint64_t total_count = 0;
    uint64_t start_time_ns, time_of_last_thresh_ns = 0, min_time_between_thresh_ns = 100e9;

    latency_and_time_t lat= {0,0,0};

    int64_t ranges [] = {100, 200, 300, 500, 1000, 2000, 3000, 5000, 7000, 10000, 20000, 30000};

    start_time_ns = get_monotonic_time_ns();

    void * hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);

    void * mstore_ctx = initialize_mstore(sizeof(latency_and_time_t), 300);




    //Set up signalatency_and_timer
    signal(SIGINT, intHandler);


    cpu_set_t  mask;
    CPU_ZERO(&mask);
    CPU_SET(7, &mask);
    int result = sched_setaffinity(0, sizeof(mask), &mask);
    if (result != 0 ) 
    {
        perror("sched_setaffinity()");
	return 0;
    }

    mlockall(MCL_CURRENT);

/*
    struct sched_attr attr;
    attr.size = sizeof(struct attr);
    attr.sched_policy = SCHED_DEADLINE;
    attr.sched_runtime = 400;
    attr.sched_period = 488;
    attr.sched_deadline = attr.sched_period - 30;

    if (sched_setattr(gettid(), &attr, 0))
        perror("sched_setattr()");
	*/

    /*
    //Set up running priority
    printf("Setting FIFO prio: %d\n", 
            sched_get_priority_max(SCHED_FIFO)); 

    struct sched_param param;
    param.sched_priority = sched_get_priority_max(SCHED_FIFO);

    if(pthread_setschedparam(pthread_self(), SCHED_FIFO, &param) != 0)
    {
        printf("Error setting schedparam.\n");
        return 0;
    }*/


    printf("Starting test run.... Hit Ctrl-C to stop.\n");


    while(keepRunning)
    {
        clock_gettime( clkType, &last_time);

	//sched_yield();
        
        /*
        if (sched_yield() != 0) 
        {
            printf("Error with sched_yield : \n");
            return 0;
        }*/

        if( getTotal_ns(&last_time) - start_time_ns < TIME_TO_SKIP_NS )
        {
    	    clock_gettime( clkType, &start_rec_time);
        }
        else
        {
            do_work(8192);

            clock_gettime( clkType, &now_time );
            lat.latency_ns = 1e9*(now_time.tv_sec - last_time.tv_sec) + 
                               now_time.tv_nsec - last_time.tv_nsec;
            lat.time_ns = getTotal_ns(&now_time);
    	    lat.run_num = total_count;

            if(lat.latency_ns < min_ns)
                min_ns = lat.latency_ns;

            if(lat.latency_ns > max_ns)
            {
                max_ns = lat.latency_ns;
                max_index = total_count;
            }

            if(lat.latency_ns > THRESHOLD_NS)
            {
                ++over_thresh_count;
                add_element(mstore_ctx, &lat);
                if(time_of_last_thresh_ns != 0 
                   && lat.time_ns - time_of_last_thresh_ns < min_time_between_thresh_ns)
                {
                   min_time_between_thresh_ns = lat.time_ns - time_of_last_thresh_ns; 
                }
                time_of_last_thresh_ns = lat.time_ns;
            }

            hist_add_element(hist_ctx, lat.latency_ns);


            total_time_ns += lat.latency_ns;
            ++total_count;

        }

	//sched_yield();
    }

    double total_time_s = (getTotal_ns(&last_time) - getTotal_ns(&start_rec_time)) / 1.0e9;


    printf("Started recording time at : %f (s), total time (s) : %f", 
           (getTotal_ns(&start_rec_time) - start_time_ns)/1.0e9,
           total_time_s
           );

    printf( "\nTotal Giga-tests: %f"
            "\nAvg: %f ns, min: %lld ns, max: %lld ns, max_in_data: %f%%\n"
            "Num over %lld ns threshold: %lld (%f%%), thats %f/s\n"
            "Min time between two successive over thresh runs (ms): %f\n", 
            total_count/1.0e9,
            ((double)total_time_ns)/total_count, 
            min_ns, 
            max_ns, 
            ((double)max_index/total_count)*100.0,
            THRESHOLD_NS,
            over_thresh_count,
            ((double)over_thresh_count/total_count)*100.0,
            over_thresh_count/total_time_s,
            min_time_between_thresh_ns/1.0e6
            );

    hist_print_stats(hist_ctx);
    hist_free(hist_ctx);

    int i;
    for(i = 0; i < get_num_elements(mstore_ctx); ++i)
    {
        latency_and_time_t * res_ptr = (latency_and_time_t*) get_element_ptr(mstore_ctx, i); 
        printf("Latency (ns) %lld, Time offset (ms) %f, loop: %lld\n", 
	        res_ptr->latency_ns, 
		(res_ptr->time_ns - start_time_ns)/1.0e6, 
		res_ptr->run_num);
    }

    free_mstore(mstore_ctx);


    return 0;
}
