#!/bin/bash
CPUID=6
SLEEP_TIME_S=601 #10 min
LOG_FILE="all_stats.txt"

echo "Starting Test..."

printf "\n=====     Start Test     =====\n\n" >> $LOG_FILE
echo `uname -r` >> $LOG_FILE

cat /proc/cmdline >> $LOG_FILE 

taskset -c $CPUID ./build/main >> $LOG_FILE &

sleep $SLEEP_TIME_S

killall -SIGINT main

sleep 1

printf "\n=====     End Test     =====\n" >> $LOG_FILE

echo "Test Complete..."
