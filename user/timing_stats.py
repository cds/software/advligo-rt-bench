
filename="trace.6"

startLine=652
endLine=706

def record_off_cpu_time(filename, print_user_runs = False):
    with open(filename) as file:
        lines = file.readlines()
        lines = [line.strip() for line in lines]

        first_exit = False
        first_line = True
        last_exit_s = 0.0
        first_time_s = 0.0
        line_num = 0

        for line in lines:
            line_num +=1
            l = line.split()

            if l[0] == "#":
                continue;

            time_s = float(l[3][:-1])

            if first_line == True:
                first_line = False
                first_time_s = time_s


            if l[4][:-1] == "user_exit":
                first_exit = True
                last_exit_s = time_s

                if print_user_runs :
                    print("Userspace Run - Start (ms) : " + str(user_start_time_s) + 
                          ", Stop (ms) : " + str(last_exit_s) + 
                        ", Dur (s): " + str((last_exit_s-user_start_time_s))  )

            elif l[4][:-1] == "user_enter":
                user_start_time_s = time_s
                print("Time (ms): " + str( (time_s - first_time_s) * 1e3) + 
                      " Off CPU (us): " + str((time_s - last_exit_s) * 1e6 ) + 
                      ", Line: "+ str(line_num))
            else:
                pass

            #print(line.split())

record_off_cpu_time(filename)

#with open(filename) as file:
#    lines = file.readlines()
#    #lines = [line.rstrip() for line in lines]
#
#    first = True
#    last_time_s = 0.0;
#
#    for i in range(startLine, endLine+1):
#        time_s = float( lines[i].split(" ")[17][:-1])
#
#        if first:
#            last_time_s = time_s
#            first = False
#        else:
#            print( str((time_s - last_time_s) * 1e6 ) + " : " +lines[i].split(" ")[18])
#            last_time_s = time_s



