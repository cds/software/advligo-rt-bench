#!/bin/bash

tuna --cpus=4-7 --isolate

# Delay the annoying vmstat timer far away
/sbin/sysctl vm.stat_interval=120

# Shutdown nmi watchdog as it uses perf events
#I dont think we have this
#/sbin/sysctl -w kernel.watchdog=0

# Pin the writeback workqueue to CPU0
find /sys/devices/virtual/workqueue -name cpumask  -exec sh -c 'echo 1 > {}' ';'

