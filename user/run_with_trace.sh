#!/bin/bash

# Full dyntick CPU on which we'll run the user loop,
# it must be part of nohz_full kernel parameter
TARGET=7

RUN_TIME_S=300

tuna --cpus=2-7 --isolate

#Max all CPU freqs
find /sys/devices/system/cpu -name scaling_governor -exec sh -c 'echo performance > {}' ';'

# Delay the annoying vmstat timer far away
/sbin/sysctl vm.stat_interval=120

# Shutdown nmi watchdog as it uses perf events
#I dont think we have this
#/sbin/sysctl -w kernel.watchdog=0

# Pin the writeback workqueue to CPU0
find /sys/devices/virtual/workqueue -name cpumask  -exec sh -c 'echo 1 > {}' ';'


sleep 1

DIR=/sys/kernel/debug/tracing
echo > $DIR/trace
echo 0 > $DIR/tracing_on
echo 0 > $DIR/events/enable #Turn off all events

# Uncomment the below for more details on what disturbs the CPU
#echo 1 > $DIR/events/irq/enable
echo 1 > $DIR/events/sched/sched_switch/enable
echo 1 > $DIR/events/workqueue/workqueue_queue_work/enable

echo 1 > $DIR/events/workqueue/workqueue_execute_start/enable
echo 1 > $DIR/events/timer/hrtimer_expire_entry/enable
echo 1 > $DIR/events/timer/tick_stop/enable

#EJ Adding More
#echo 1 > $DIR/events/thermal/enable
#echo 1 > $DIR/events/migrate/enable
#echo 1 > $DIR/events/sched/enable
#echo 1 > $DIR/events/signal/enable
#echo 1 > $DIR/events/enable #Turn on all events

echo nop > $DIR/current_tracer
echo 1 > $DIR/tracing_on

# Run aX secs user loop on target
taskset -c $TARGET ./build/main &
sleep $RUN_TIME_S
killall -SIGINT main

# Checkout the trace in trace.* file
cat /sys/kernel/debug/tracing/per_cpu/cpu$TARGET/trace > trace.$TARGET

echo 0 > $DIR/tracing_on
