# Adv. LIGO Real-time Benckmark
This repository contains benchmarking code for the real-time functions needed for LIGO's front end (FE) controllers. It contains tests for the [kernel space](https://git.ligo.org/cds/software/advligorts/-/tree/master/src/drv/rts-cpu-isolator) isolation module approach, as well as userspace benchmarks with some scripts for tracing interruptions.

The LIGO front end controllers need a ***no*** interruption solution to isolation. The kernel space isolation approach allows for very little jitter in FE control loops (less than ~3us). This is currently the selected solution as userspace options still tend to have high latency interruptions (~12 us [If hardware/BIOS are configured correctly]).

## Structure

### `kernel/`
The kernel benchmark module can be build with `make`. You will need your kernel headers installed and the [rts-cpu-isolator](https://git.ligo.org/cds/advligorts/-/tree/master/src/drv/rts-cpu-isolator) module installed. The makefile also assumes that the [advligorts](https://git.ligo.org/cds/advligorts) repository has been cloned into the same parent directory as this repository.

#### Kernel Module Usage
```
make
sudo insmod advligorts-rt-bench.ko
#Wait for however long you want to run the test
sudo rmmod advligorts-rt-bench.ko

sudo dmesg #View results on rmmod
```

### `user/`
The user space benchmark can be built by running `make`

The `run_with_trace.sh` script has an example of how you would isolate cores and run your task on a single CPU.
