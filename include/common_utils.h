#ifndef COMMON_UTILS_H
#define COMMON_UTILS_H



#ifdef __KERNEL__
#include <linux/slab.h>
#include <asm/tsc.h> //tsc_khz

static inline void* common_malloc(uint64_t size)
{
    return kmalloc(size, GFP_NOWAIT);
}

static inline void common_free(void * ptr)
{
    kfree(ptr);
}


/** @brief Fills the uint64_t * with the start time recorded
 *         when this function is called.
 *
 *  @param start_tsc [out] This is filled with the current time (tsc)
 *
 *  @return None
 */
static inline void timer_start( uint64_t * start_tsc)
{
    *start_tsc = rdtsc_ordered();
}

/** @brief Returns the time elapsed from when the start_tsc
 *         was last filled with timer_start()
 *
 *  @param start_tsc [in] The start time you are using to
 *                       calculate the elapsed time (tsc)
 *
 *  @return The time elapsed from when the start_tsc was
 *          filled to now, in ns
 */
static inline uint64_t timer_tock_ns( uint64_t * start_tsc)
{
    return ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
}

/** @brief Returns the time elapsed from when the start_tsc
 *         was last filled with timer_start(), and also stores
 *         that result in the start_tsc parameter
 *
 *  @param start_ns [in/out] This is filled with duration (ns)
 *                           between the time stored in start_tsc when first
 *                           passed in and now.
 *
 *  @return Returns the elapsed time from that start time passed in and now
 */

static inline uint64_t timer_end_ns( uint64_t * start_tsc)
{
    *start_tsc = ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
    return *start_tsc; //This is now the duration in ns
}

static inline uint64_t timer_diff_ns ( uint64_t * end_tsc, uint64_t * start_tsc)
{
    return ((*end_tsc - *start_tsc) * 1000000ULL ) / tsc_khz ;
}


#define common_print printk

#else //User Mode
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

static inline void* common_malloc(uint64_t size)
{
    return malloc(size);
}

static inline void common_free(void * ptr)
{
    free(ptr);
}

static inline uint64_t get_monotonic_time_ns( void )
{
    static struct timespec cur_time;
    clock_gettime( CLOCK_MONOTONIC, &cur_time );
    return cur_time.tv_sec*1e9 + cur_time.tv_nsec;;
}


#define common_print printf

#endif //ifdef __KERNEL__


#endif
