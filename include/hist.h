#ifndef LIGO_HIST_H
#define LIGO_HIST_H


#include "common_utils.h"


#ifdef __KERNEL__
#include <linux/string.h>
#else
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#endif



typedef struct hist_context_t
{
    int num_ranges;
    int64_t * ranges;
    int64_t * range_counts;
} hist_context_t;

static void hist_free(hist_context_t *);

//static hist_context_t * hist_initialize(int num_ranges,  

static hist_context_t * hist_initialize(int num_ranges, int64_t * range_array)
{
    uint64_t i = 0;
    hist_context_t * context;

    if(num_ranges < 2)
    {
        common_print("hist_initialize() - You must pass in at least two numbers as range bounds.\n");
        return NULL;
    }
    ++num_ranges; //We add one to store elements higher than the largest bound

    context = (hist_context_t *) common_malloc(sizeof(hist_context_t));
    if(context == NULL)
        return NULL;

    context->ranges = (int64_t*) common_malloc( sizeof(int64_t) * num_ranges);
    if(context->ranges == NULL)
    {
        common_free(context);
        return NULL;
    }

    context->range_counts = (int64_t*) common_malloc( sizeof(int64_t) * num_ranges);
    if(context->range_counts == NULL)
    {
        common_free(context->ranges);
        common_free(context);
        return NULL;
    }
    memset(context->range_counts, 0, sizeof(int64_t) * num_ranges);

    context->num_ranges = num_ranges;

    for(i = 1; i < num_ranges-2; ++i)
    {
        if(range_array[i-1] >= range_array[i])
        {
            common_print("hist_initialize() - The range array parameter must be in ascending order.\n");
            hist_free(context);
            return NULL;
        }
        context->ranges[i-1] = range_array[i-1];
    }
    context->ranges[i-1] = range_array[i-1];
    context->ranges[i] = range_array[i];
    context->ranges[i+1] = range_array[i];

    return context;
}

static void hist_add_element(hist_context_t * context, int64_t ele)
{
    uint64_t i; 
    for(i = 0; i < context->num_ranges-1; ++i)
    {
        if(ele < context->ranges[i])
        {
            context->range_counts[i] += 1;
            return;
        }
    }

    //If we got here we are higher than the largest range
    context->range_counts[i] += 1;

    return;
}

static inline int64_t hist_get_range_count(hist_context_t * context, int64_t index)
{
    if(index >= context->num_ranges)
    {
        common_print("get_range() - index parameter was out of range.\n");
        return -1;
    }

    return context->range_counts[index];
}

static inline int hist_get_num_ranges(hist_context_t * context)
{
    return context->num_ranges;
}

static inline int64_t hist_get_range_from_index(hist_context_t * context, int64_t index)
{
    if(index >= context->num_ranges)
    {
        common_print("get_range_from_index() - index parameter was out of range.\n");
        return -1;
    }
    return context->ranges[index];

}

static void hist_print_stats(hist_context_t * context)
{
    int64_t i;
    common_print("<%lld : %lld\n",context->ranges[0], context->range_counts[0]);
    for(i = 1; i < context->num_ranges-1; ++i)
    {
        common_print("[%lld, %lld) : %lld\n",context->ranges[i-1], context->ranges[i], context->range_counts[i] );
    }
    common_print(">%lld : %lld\n", context->ranges[context->num_ranges-1], context->range_counts[context->num_ranges-1]);
}

static void hist_free(hist_context_t * context)
{
    if ( context == NULL ) return;
    common_free(context->range_counts);
    common_free(context->ranges);
    common_free(context);
    context = 0;
    return;
}

#endif

